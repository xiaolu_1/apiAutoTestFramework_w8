import requests


# 定义接口类 ---- 接口对象层
class IhrmEmpManage(object):
    # 定义接口方法
    def add_emp(self, req_header, req_body):
        resp = requests.post(url="http://ihrm2-test.itheima.net/api/sys/user",
                             headers=req_header, json=req_body)
        return resp

    def modify_emp(self, emp_id, req_header, req_body):
        resp = requests.put(url="http://ihrm2-test.itheima.net/api/sys/user/" + emp_id,
                            headers=req_header, json=req_body)
        return resp

    def query_emp(self, emp_id, req_header):
        return requests.get(url="http://ihrm2-test.itheima.net/api/sys/user/" + emp_id, headers=req_header)

    def delete_emp(self, emp_id, req_header):
        return requests.delete(url="http://ihrm2-test.itheima.net/api/sys/user/" + emp_id, headers=req_header)


# 必须 自测
if __name__ == '__main__':
    # 创建实例
    ihrm = IhrmEmpManage()
    # 请求头
    req_header = {"Authorization": "b9c8f43a-c406-4af8-b2e8-0e1a04fd6ae3"}
    # 请求体
    req_data = {
        "username": "jack123",
        "mobile": "18984893601",
        "workNumber": "9527"
    }
    # 实例调用方法 - 添加
    resp = ihrm.add_emp(req_header, req_data)
    print("添加员工：", resp.json())

    # 员工id
    emp_id = "165041495529"
    # 修改员工的 请求体
    req_body = {"username": "刘大锤！"}
    # 实例调用方法 - 修改
    resp = ihrm.modify_emp(emp_id, req_header, req_body)
    print("修改员工：", resp.json())
