import logging
import jsonschema
from api.ihrm_login import IhrmLogin
from common.assert_tools import common_assert


class TestLogin(object):
    """定义 测试类"""

    def setup_class(self):
        # 创建实例
        self.ihrm_login = IhrmLogin()

    # 定义测试方法 -- 对应 测试用例
    def test01_login_ok(self):
        # 准备测试数据 -- 与当前方法名对应。
        req_data = {"mobile": "13800000002", "password": "123456"}
        # 调用自己封装的 api，发送 登录接口请求
        resp = self.ihrm_login.login(req_data)
        # 打印
        # print("登录成功：", resp.json())
        logging.info(f"登录成功：{resp.json()}")

        # 断言
        assert 200 == resp.status_code
        # assert True is resp.json().get("success")
        # assert 10000 == resp.json().get("code")
        # assert "操作成功" in resp.json().get("message")
        # common_assert(resp, 200, True, 10000, "操作成功")
        # 指定校验规则
        schema = {
            "type": "object",
            "properties": {
                "success": {"const": True},
                "code": {"const": 10000},
                "message": {"pattern": "^操作成功"},
                "data": {"type": "string"}
            },
            "required": ["success", "code", "message", "data"]
        }
        # 校验
        jsonschema.validate(instance=resp.json(), schema=schema)

    def test02_mobile_not_reg(self):
        # 准备测试数据 -- 与当前方法名对应。
        req_data = {"mobile": "13843718436", "password": "123456"}
        # 调用自己封装的 api，发送 登录接口请求
        resp = self.ihrm_login.login(req_data)
        # 打印
        print("手机号未注册：", resp.json())
        # 断言
        common_assert(resp, 200, False, 20001, "用户名或密码错误")

    def test03_pwd_err(self):
        # 准备测试数据 -- 与当前方法名对应。
        req_data = {"mobile": "13800000002", "password": "123676"}
        # 调用自己封装的 api，发送 登录接口请求
        resp = self.ihrm_login.login(req_data)
        # 打印
        print("密码错误：", resp.json())
        # 断言
        common_assert(resp, 200, False, 20001, "用户名或密码错误")

    def test04_12_mobile(self):
        # 准备测试数据 -- 与当前方法名对应。
        req_data = {"mobile": "138000000023", "password": "123676"}
        # 调用自己封装的 api，发送 登录接口请求
        resp = self.ihrm_login.login(req_data)
        # 打印
        print("12位手机号：", resp.json())
        # 断言
        common_assert(resp, 200, False, 20001, "用户名或密码错误")

    def test05_10_moblie(self):
        # 准备测试数据 -- 与当前方法名对应。
        req_data = {"mobile": "1380000000", "password": "123456"}
        # 调用自己封装的 api，发送 登录接口请求
        resp = self.ihrm_login.login(req_data)
        # 打印
        print("19位手机号：", resp.json())
        # 断言
        common_assert(resp, 200, False, 20001, "用户名或密码错误")

    def test06_more_params(self):
        req_data = {"mobile": "13800000002", "password": "123456", "abc": "123"}
        resp = self.ihrm_login.login(req_data)
        common_assert(resp, 200, True, 10000, "操作成功")

    def test07_less_params(self):
        req_data = {"password": "123456"}
        resp = self.ihrm_login.login(req_data)
        common_assert(resp, 200, False, 20001, "用户名或密码错误")

    def test08_none_parmes(self):
        req_data = None
        resp = self.ihrm_login.login(req_data)
        common_assert(resp, 200, False, 99999, "抱歉，系统繁忙，请稍后重试！")

    def test09_err_params(self):
        req_data = {"mobile": "13800000002", "abc": "123456"}
        resp = self.ihrm_login.login(req_data)
        common_assert(resp, 200, False, 20001, "用户名或密码错误")
