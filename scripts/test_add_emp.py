from config import TEL
from common.db_tools import DBTools
from api.emp_manage import IhrmEmpManage
from common.assert_tools import common_assert
from common.get_req_header import get_req_header


class TestAddEmp:
    """添加员工测试类"""

    def teardown(self):
        pass
        # 删除 使用的 手机号
        DBTools.iud(f"delete from bs_user where mobile = '{TEL}';")

    def setup_class(self):
        # 创建 IhrmEmpManage 实例
        self.ihrm = IhrmEmpManage()

        # 调用自己封装的函数 获取请求头 - 1组够用！
        self.req_header = get_req_header()

        # 删除 delete 要使用的 手机号
        DBTools.iud(f"delete from bs_user where mobile = '{TEL}';")

    def test01_must_params(self):
        # 准备请求体
        req_data = {
            "username": "jack123",
            "mobile": TEL,
            "workNumber": "9527"
        }
        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("添加成功（必选）：", resp.json())
        # 断言
        common_assert(resp, 200, True, 10000, "操作成功")

    def test02_all_params(self):
        # 准备请求体
        req_data = {
            "username": "唐伯虎7",
            "mobile": TEL,
            "formOfEmployment": 1,
            "workNumber": "9527",
            "departmentName": "测试某7部",
            "timeOfEntry": "2023-03-31",
            "correctionTime": "2023-04-29"
        }

        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)

        # 打印 响应结果
        print("添加成功（全部）：", resp.json())

        # 断言
        common_assert(resp, 200, True, 10000, "操作成功")

    def test03_mobile_exists(self):
        # 准备请求体
        req_data = {
            "username": "jack123",
            "mobile": "13800000002",  # 不能使用 setup/teardown 删除的手机号
            "workNumber": "9527"
        }

        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("添加失败（手机号已存在）：", resp.json())
        # 断言
        common_assert(resp, 200, False, 20002, "新增员工失败")

    def test04_username_none(self):
        # 准备请求体
        req_data = {
            "username": None,
            "mobile": TEL,
            "workNumber": "9527"
        }

        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("添加失败（用户名为空）：", resp.json())
        # 断言
        common_assert(resp, 200, False, 20002, "新增员工失败")

    def test05_more_params(self):
        # 准备请求体
        req_data = {
            "username": "jack123",
            "mobile": TEL,
            "workNumber": "9527",
            "abc": "123"
        }
        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("多参（多abc）：", resp.json())
        # 断言
        common_assert(resp, 200, True, 10000, "操作成功")

    def test06_no_params_dept_id(self):
        req_data = {
            "departmentId": "9527"
        }
        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("无参(有可选部门id)：", resp.json())
        # 断言
        common_assert(resp, 200, False, 20002, "新增员工失败")

    def test07_no_params(self):
        req_data = None
        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("无参：", resp.json())
        # 断言
        common_assert(resp, 200, False, 99999, "抱歉，系统繁忙，请稍后重试！")
