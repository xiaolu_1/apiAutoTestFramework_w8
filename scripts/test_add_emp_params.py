import pytest

from common.read_json_tools import read_json_data
from config import TEL
from common.db_tools import DBTools
from api.emp_manage import IhrmEmpManage
from common.assert_tools import common_assert
from common.get_req_header import get_req_header

"""
1. 将测试数据，组织到 json文件中
2. 读取json文件中的数据，转换成 [(),(),()] 
3. 在 通用测试方法上一行，添加装饰器 @pytest.mark.parametrize()
4. 给 parametrize()添加参数。 参1：1个字符串， 参2：[(),(),()] 
5. 给 通用测试方法 添加形参。 为 parametrize()参1的内容
6. 在 通用测试方法 内使用形参。
"""

# 读json文件，获取元组列表数据
data = read_json_data("add_emp_data.json")


class TestAddEmpParams:
    """添加员工测试类"""

    def teardown(self):
        # 删除 使用的 手机号
        DBTools.iud(f"delete from bs_user where mobile = '{TEL}';")

    def setup_class(self):
        # 创建 IhrmEmpManage 实例
        self.ihrm = IhrmEmpManage()

        # 调用自己封装的函数 获取请求头 - 1组够用！
        self.req_header = get_req_header()

        # 删除 delete 要使用的 手机号
        DBTools.iud(f"delete from bs_user where mobile = '{TEL}';")

    # 通用测试方法
    @pytest.mark.parametrize("desc, req_data, status_code, success, code, message", data)
    def test_add_emp(self, desc, req_data, status_code, success, code, message):

        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("添加成功（必选）：", resp.json())
        # 断言
        common_assert(resp, status_code, success, code, message)
