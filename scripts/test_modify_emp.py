from api.emp_manage import IhrmEmpManage
from common.assert_tools import common_assert
from common.db_tools import DBTools
from common.get_req_header import get_req_header


class TestModifyEmp(object):
    def setup(self):
        # 添加员工id
        DBTools.iud("insert into bs_user(id, mobile, username) values('1122334455', '18936723725', 'jfd');")

    def teardown(self):
        # 删除员工id
        DBTools.iud("delete from bs_user where id = '1122334455';")

    # 定义测试方法
    def test_modify_emp_success(self):
        # 创建实例
        ihrm = IhrmEmpManage()
        # 员工id
        emp_id = "1122334455"
        req_headers = get_req_header()
        req_body = {"username": "发JDK"}
        # 调用自己封装的api， 获取响应结果
        resp = ihrm.modify_emp(emp_id, req_headers, req_body)

        # 打印、断言
        print("修员工：", resp.json())
        common_assert(resp, 200, True, 10000, "操作成功")


    def test_emp_id_none(self):
        pass
