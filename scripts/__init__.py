from config import BASE_DIR
from common.logging_tools import init_log_config

init_log_config(BASE_DIR + "/log/" + "ihrm.log")
