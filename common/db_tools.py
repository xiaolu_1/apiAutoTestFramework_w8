import pymysql

"""
通常情况下，工具类中的方法，为了方便调用，可以制作成 静态方法、类方法。
1. 添加装饰器
2. 修改参数
"""


# 创建数据库工具类
class DBTools(object):
    # 创建连接方法 - 静态方法 - 没有参数
    @staticmethod
    def __create_connet():
        conn = pymysql.connect(host="211.103.136.244", port=7061, user="student", password="iHRM_student_2022",
                               database="ihrm", charset="utf8")
        return conn

    # 查询 一条 方法
    @staticmethod
    def query_one(sql):
        conn = None
        cursor = None
        res = None
        try:
            # 创建连接
            conn = DBTools.__create_connet()
            # 创建游标
            cursor = conn.cursor()
            # 执行sql -- 查
            cursor.execute(sql)
            # 提取结果：查询，获取 第一条 数据
            res = cursor.fetchone()  # res  --- result
        except Exception as err:
            print("执行查询sql错误：", err)
            raise
        finally:
            # 关闭游标
            cursor.close()
            # 关闭连接
            conn.close()
            return res

    # 查询方法
    @staticmethod
    def query_many(sql, idx, n):
        """
        :param sql:
        :param idx: 查询第几条起始
        :param n: 查询多少条
        :return:
        """
        conn = None
        cursor = None
        res = None
        try:
            # 创建连接
            conn = DBTools.__create_connet()
            # 创建游标
            cursor = conn.cursor()
            # 执行sql -- 查
            cursor.execute(sql)
            # 修改游标位置
            cursor.rownumber = idx - 1
            # 提取结果：查询，获取 n条 数据
            res = cursor.fetchmany(n)  # res  --- result
        except Exception as err:
            print("执行查询sql错误：", err)
            raise
        finally:
            # 关闭游标
            cursor.close()
            # 关闭连接
            conn.close()
            return res

    # 增删改方法
    @staticmethod
    def iud(sql):
        conn = None
        cursor = None
        try:
            # 创建连接
            conn = DBTools.__create_connet()
            # 创建游标
            cursor = conn.cursor()
            # 执行sql -- 增、删、改
            res = cursor.execute(sql)
            # 提交事务
            conn.commit()
            print(f"增、删、改操作成功！影响：{res}行")
        except Exception as err:
            conn.rollback()
            print("执行增删改sql错误：", err)

            raise
        finally:
            # 关闭游标
            cursor.close()
            # 关闭连接
            conn.close()


if __name__ == '__main__':
    DBTools.idu("insert xxxx")

    ret = DBTools.query_one("select is_delete from bs_user where id = '1650402744906821632';")
    # ret = DBTools.query_many("select * from t_book;", 3, 2)
    print(ret)

    # DBTools.iud("insert into t_book(id, title, pub_date) values(117, '神雕侠侣', '1986-01-01');")
    # DBTools.iud("update t_book set `read` = `read` + 1 where title = '神雕侠侣';")
