import json
from config import BASE_DIR


def read_json_data(filename):
    """读取json文件数据，转换成元组列表"""
    # 打开文件
    with open(BASE_DIR + "/data/" + filename, "r", encoding="utf8") as f:
        # 读取json数据
        json_data = json.load(f)

        # 在 for 之前，定义空列表
        list_data = list()

        # 遍历json数据，提value值，转换元组，追加到列表
        for dict1 in json_data:
            # val = dict1.values()
            # tmp = tuple(val)
            # list_data.append(tmp)
            list_data.append(tuple(dict1.values()))

        # 在 for 循环以外，返回列表
        return list_data


# 必须自测
if __name__ == '__main__':
    # ret = read_json_data("../data/login_data.json")  # 相对路径
    # ret = read_json_data("login_data.json")  # 相对路径
    ret = read_json_data("add_emp_data.json")
    print(ret)
