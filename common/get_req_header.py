import requests


def get_req_header():
    # 正确手机号、密码 登录成功， 返回
    resp = requests.post(url="http://ihrm2-test.itheima.net/api/sys/login",
                         json={"mobile": "13800000002", "password": "123456"})
    # 提取 data 值，
    token = resp.json().get("data")

    # 拼接成 请求头 返回
    return {"Authorization": token}


# 必须 自测
if __name__ == '__main__':
    header = get_req_header()
    print(header)
